<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    protected $fillable = ['query_id' , 'doctor_name' , 'answers'];

    public function question(){
        return $this->belongsTo('App\Question');
    }
}
