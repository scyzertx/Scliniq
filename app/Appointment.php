<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{
    protected $fillable=['doctor_id', 'user_id', 'hospital_id' ,'department_id', 'from' , 'to' , 'date'];

    public function doctor(){
        return $this->belongsTo('App\Doctor');
    }

    public function hospital(){
        return $this->belongsTo('App\Hospital');
    }

    public function department(){
        return $this->belongsTO('App\Department');
    }
}
