<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $fillable=['name', 'hospital_id'];

    public function appointments(){
        return $this->hasMany('App\Appointment');
    }

    public function hospital(){
        return $this->belongsTo('App\Hospital');
    }

    public function doctor(){
        return $this->hasOne('App\Doctor');
    }
}

