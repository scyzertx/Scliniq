<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Doctor extends Model
{
    protected $fillable=['name','address','contact_no','nmc_no' , 'hospital_id' , 'department_id'];

    public function appointments(){
        return $this->hasMany('App\Appointment');
    }

    public function hospital(){
        return $this->belongsTo('App\Hospital');
    }

    public function department(){
        return $this->belongsTo('App\Department');
    }
    
}
