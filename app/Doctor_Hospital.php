<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Doctor_Hospital extends Model
{
    protected $table='doctors_hospitals';

    protected $fillable=['doctor_id' , 'hospital_id' , 'department_id'];

    public function department_name(){
        return $this->belongsTo('App\Department');
    } 
}
