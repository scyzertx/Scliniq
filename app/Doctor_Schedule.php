<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Doctor_Schedule extends Model
{
    protected $table = 'doctors_schedules';
    
    protected $fillable= ['doctor_id' , 'hospital_id' , 'department_id' , 'date' , 'from' , 'to'];
}
