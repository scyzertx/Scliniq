<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FirstAidDescription extends Model
{
    public $table='firstaid_descriptions';

    protected $fillable = ['firstaid_id' , 'descriptions'];

    public function firstaid(){
        return $this->belongsTo('App\Firstaid');
    }
}
