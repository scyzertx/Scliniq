<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Firstaid extends Model
{
    protected $fillable=['name'];

    public function details(){
        return $this->hasMany('App\FirstAidDescription');
    }
}
