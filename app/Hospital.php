<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hospital extends Model
{
    protected $fillable=['name','address' , 'images' , 'latitude' , 'longitude' , 'contact_no' , 'description'];

    public function appointments(){
        return $this->hasMany('App\Appointment');
    }
    public function doctors(){
            return $this->hasMany('App\Doctor');
    }

    public function departments(){
        return $this->hasMany('App\Department');
    } 

    public function admin(){
        return $this->hasOne('App\User');
    }
    
}

    

