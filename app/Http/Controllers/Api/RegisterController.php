<?php

namespace App\Http\Controllers\Api;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use App\VerifyUser;
use App\Mail\VerifyMail;
use Illuminate\Support\Facades\Mail;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    // protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
            'phone_number'=>'required|numeric',
        ]);
        
        if($request->wantsJson()){
            if($validator->fails()){
                return response()->json([
                    'status' => 'fail',
                    'message' => 'validation fails',
                    'data' => $validator->errors()
                ]);
            }

            $user = User::create([
                'name' => $request['name'],
                'email' => $request['email'],
                'password' => bcrypt($request['password']),
                'phone_number'=>$request['phone_number'],
                'f_token' => $request['f_token'],
            ]);

            $verifyUser = VerifyUser::create([
                'user_id' => $user->id,
                'token'=>str_random(40),
    
            ]);
    
            Mail::to($user->email)->send(new VerifyMail($user));
    

            return response()->json([
                'status' => 'success',
                'message' => 'User Registration success',
                'data' => $user
            ]);
        }
       
        
    }
    public function sendAppointmentNotification(Request $req, $token)
    {$optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60*20);

        /// select * from schedule where time = currentime
        // record -> filter -> token == token
        
        $notificationBuilder = new PayloadNotificationBuilder('my title');
        $notificationBuilder->setBody('Hello world')
                            ->setSound('default');
        
        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData(['title' => 'my title', 'message' => 'message']);
        
        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();
        
                
        $downstreamResponse = FCM::sendTo("dvGDhXE19jM:APA91bGOVhPtwKbRZhxFc69tGUBC7EEXSiMmw6L95BkqMynvloM5UVN3_FXQL92YKq9fP-AESq1DzKNNNjJN1NniwEKqYMMueXK8X5ii2pX0eofZzVHHWCnQxN2szYbjil8ftxsrA_4K", $option, $notification, $data);
        
        var_dump($downstreamResponse->numberSuccess());
        var_dump($downstreamResponse->numberFailure());
        var_dump($downstreamResponse->numberModification());

    }

    public function sendMedicineRemainder($token)
    {
        $msg = array
        (
            'body' => 'Time to take Medicine',
            'title' => 'Medicine Remainder'
        );
        $fields = array
        (
            'to' => $token,
            'notification' => $msg
        );


        $headers = array
        (
            'Authorization: key=AAAAIGIxPKE:APA91bHvBzSRzXfRKVtuiji5uXhLYn_dEv3qyFqvnIDSygYbIadJkmwcg6WboUJjUOjKZ_H1leHZxbmsGw9zAaUodjgXI9wXZhPZ-b326MTbB537y8RycVGRzHT8Lt4rxZt0ZZqrME1q',
            'Content-Type: application/json'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }


    
}
