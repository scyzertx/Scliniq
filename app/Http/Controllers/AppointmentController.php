<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Appointment;
use App\Hospital;
use App\Doctor;
use App\Department;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Doctor_Schedule;

class AppointmentController extends Controller
{

    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->admin == 2){
            $appointments = Appointment::where('hospital_id' , Auth::user()->hospital->id)->get();
        
        }else{
            $appointments = Appointment::all();
        }

        return view('appointments.index' ,compact('appointments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $selected_doctor = Doctor::find($id);
        
        $departments = Department::all();
        $doctors = Doctor::all();
        $hospitals = Hospital::all();
        return view('appointments.create' , compact('hospitals','doctors', 'departments' ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
       
        $current_date_time = Carbon::now();
        $date = $request['date'];
        

        if($request->wantsJson()){
            $user_id_check = $request['user_id'];
        }else{
            $user_id_check = Auth::user()->id;
        }

        if($request->wantsJson()){
            $from = $request['from'];
            $check = Appointment::where('from' , $from)->where('date' , $date)->where('user_id' , $user_id_check)->get();
        }else{
            $from = explode(' ' , $request['shifts']);
            $check = Appointment::where('from' , $from[0])->where('date' , $date)->where('user_id' , $user_id_check)->get();
        }
    
     
        if($check->count() == 0){
            if($request->wantsJson()){

                $add_new=Appointment::create($request->all());
                return response()->json([
                    'status' => 'success',
                    'message' => 'appointment',
                    'data' => $add_new
                ]);
    
            }
            
            $from_to = preg_split('/\s+/', $request['shifts']);
            // $request['time']=Carbon::createFromFormat('H:i a' , $request->time);
            // $request['date']=Carbon::createFromFormat('m/d/Y' , $request->date);
            $request['user_id']= Auth::user()->id;
            $request['from'] = $from_to[0];
            $request['to'] = $from_to[1];
            $add_new=Appointment::create($request->all());
          
            return redirect('/appointments');
        }else{
            if($request->wantsJson()){

                return response()->json([
                    'status' => 'fail',
                    'message' => 'appointment already set',
                ]);
    
            }

            return redirect()->back();
        
            
        }
        
        

        // $current_time= $current_date_time->format('H:i:s');
        // $current_date= $current_date_time->format('Y-m-d');
        // if($request['date'] >= $current_date ){
        //     return redirect()->back();

        //     if($request->wantsJson()){
        //         return response()->json([

        //         ]);
        //     }
        // }
        // dump($current_time);
        // dd($current_date);
        
         
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $appointment= Appointment::find($id);
        $doctors = Doctor::all();
        $hospitals = Hospital::all();
        $departments = Department::all();
        return view('appointments.edit' , compact('appointment' , 'doctors' , 'hospitals', 'departments'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request['time']=Carbon::createFromFormat('H:i a' , $request->time);
        $request['date']=Carbon::createFromFormat('m/d/Y' , $request->date);
        $old_Appointment= Appointment::find($id);
        $new_appointment= $request->all();
        $old_Appointment->fill($new_appointment)->save();
        return redirect ('/appointments'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request , $id)
    {
        $appointment=Appointment::find($id);

        if(empty($appointment)){
            return response()->json([
                'status' => 'failed',
                'message' => 'no appointment found',
            
            ]);


        }

        $appointment->delete();
        if($request->wantsJson()){
            return response()->json([
                'status' => 'success',
                'message' => 'appointment cancled',
            
            ]);

        }

        return redirect('/appointments');
    }

    public function show_all($id){
    
        $appointments = Appointment::where('hospital_id' , $id)->get();

        return view('appointments.index' ,compact('appointments'));
    }

    public function show_appointment($id){
        
        $selected_doctor = Doctor::find($id);
        $selected_department = $selected_doctor->department;
        $selected_hospital = Hospital::where('id' , $selected_doctor->hospital_id)->first();

        $doctors = Doctor::where('hospital_id' , $selected_hospital->id)->get();
        $departments = Department::where('hospital_id' , $selected_hospital->id)->get();
        $hospitals = Hospital::all();

        return view('appointments.appointment' , compact('selected_doctor' , 'selected_department' , 'selected_hospital' , 'doctors' , 'departments' , 'hospitals'));
    }

    public function appointment_show_details(Request $request , $id){
        
        $appointments = Appointment::where('user_id' , $id)->get();
        $new_appointments = $appointments;
        foreach($new_appointments as $appointment){
            $hospital = Hospital::where('id' , $appointment['hospital_id'])->first();
            $appointment['hospital'] = $hospital;
            $department = Department::where('id' , $appointment['department_id'])->first();
            $appointment['department']= $department;
            $doctor = Doctor::where('id' , $appointment['doctor_id'])->first();
            $appointment['doctor'] = $doctor;    
        }
        
        if($request->wantsJson()){
            return response()->json([
                'status' => 'success',
                'message' => 'Appointments details',
                'data' => $new_appointments
            ]); 
        }
        

    }

    public function specific_appointments(Request $request)
    {
        $hospital = Hospital::find($request['hospital_id']);
        $department = Department::find($request['department_id']);
        $doctor = Doctor::where('hospital_id' , $request['hospital_id'])->where('department_id' , $request['department_id'])->first();

        $schedules = Doctor_Schedule::where('doctor_id' , $doctor->id)->get();

        if($request->wantsJson()){
            return response()->json([
                'status' => 'success',
                'message' => 'Specific List',
                'data' => ['hospital' => $hospital  , 'department' => $department , 'doctor' => $doctor , 'schedules' => $schedules]
            ]);
        }
    }
}