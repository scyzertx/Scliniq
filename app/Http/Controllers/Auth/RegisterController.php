<?php

namespace App\Http\Controllers\Auth;

use App\Mail\VerifyMail;
use App\User;
use App\Http\Controllers\Controller;
use App\VerifyUser;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'phone_number'=>'required|numeric',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'name' => $data['name'],
            'phone_number'=>$data['phone_number'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),

        ]);

        $verifyUser = VerifyUser::create([
            'user_id' => $user->id,
            'token'=>str_random(40),

        ]);

        Mail::to($user->email)->send(new VerifyMail($user));

        return $user;

    }

    public function verifyUser($token , Request $request)
    {
        $verify_user = VerifyUser::where('token' , $token)->first();
        if(isset($verify_user)){
            if(!$verify_user->user->verified)
            {
                $verify_user->user->verified = 1;
                $verify_user->user->save();
                $status='Your email is verified. You can now login using android-app or web-app';
            }else{
                $status="You are already verified . You can login in now";
            }
        }else{
            $error="Your email is not identified";
            if($request->wantsJson()){
                return response()->json(['status'=>'fail' , 'message'=>$error]);
            }
            Session::flash('error' , $error);
            return redirect('/');
        }

        if($request->wantsJson()){
            return response()->json(['status'=>'success' , 'message'=>$status , 'data'=>$verify_user]);
        }

        Session::flash('success' , $status);
        return redirect('/');

    }

    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        Session::flash('success' , 'We have sent you an activation code.Check your email and click on the link to verify');

        return redirect('/');

    }


    public function resend($id){
        try{
            $user = User::findorfail($id);
            Mail::to($user->email)->send(new VerifyMail($user));
            Session::flash('success' , 'Mail sent successfully');
            return redirect('/');

        }catch(\Exception $e){
            Session::flash('warning' , 'Mail not sent');
            return redirect('/');
        }

    }


}
