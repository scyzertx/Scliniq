<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Department;
use App\Hospital;
use Illuminate\Support\Facades\Auth;


class DepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(Auth::user()->admin == 2){
            $departments = Department::where('hospital_id' , Auth::user()->hospital->id)->get();
        
        }else{
            $departments = Department::all();
        }

        if($request->wantsJson()){
            return response()->json([
                'status' => 'success',
                'message' => 'department list',
                'data' => $departments
            ]); 
        }
        

        return view('departments.index' ,compact('departments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Auth::user()->admin == 2){
            $hospitals = Hospital::where('id' , Auth::user()->hospital->id)->get();
        
        }else{
            $hospitals = Hospital::all();
        }  
        
        return view('departments.create', compact('hospitals'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $add_new=department::create($request->all());
      
        return redirect('/departments'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $department= department::find($id);
        $hospitals = Hospital::all();
        return view('departments.edit' , compact('department','hospitals'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $old_department= department::find($id);
        $new_department= $request->all();
        $old_department->fill($new_department)->save();
        return redirect ('/departments');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $department=department::find($id);
        $department->delete();
        return redirect('/departments');
    }
}
