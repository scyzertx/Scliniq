<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Doctor;
use Illuminate\Support\Facades\Auth;
use App\Department;
use App\Doctor_Hospital;
use App\User;
use App\Doctor_Schedule;
use App\Hospital;
use Carbon\Carbon;


class DoctorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
   
        if(Auth::user()->admin == 2){
            $doctors = Doctor::where('hospital_id' , Auth::user()->hospital->id)->get();
        
        }else{
            $doctors = doctor::all();
        }

        if($request->wantsJson()){
            return response()->json([
                'status' => 'success',
                'message' => 'doctort list',
                'data' => $doctors
            ]); 
        }

        return view('doctors.index' ,compact('doctors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $hospital_id=Auth::user()->hospital_id;
        $departments=Department::where('hospital_id' , $hospital_id)->get();
        $doctors = User::where('admin' , 3)->get();
        return view('doctors.create', compact('departments' , 'doctors'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      $add_new_doctor=Doctor::create($request->all());
      
        return redirect('/doctors'); 

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $doctor= Doctor::find($id);
        return view('doctors.edit' , compact('doctor'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $old_doctor= Doctor::find($id);
        $new_doctor= $request->all();
        $old_doctor->fill($new_doctor)->save();
        return redirect ('/doctors'); 

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $doctor=Doctor::find($id);
        $doctor->delete();
        return redirect('/doctors');
    }

    public function show_all($id){
        $doctors = Doctor::where('id' , $id)->get();

        return view('doctors.index' ,compact('doctors'));
    }

    public function show_details(Request $request , $id){
        
        $doctor = Doctor::find($id);
        
        $hospital = Hospital::where('id' , $doctor->hospital_id)->first();

        $department = Department::where('id' , $doctor->department_id)->first();
       

        if($request->wantsJson()){
            return response()->json([
                'status' => 'success',
                'message' => 'Details list',
                'data' => ['doctor' => $doctor , 'hospital'=> $hospital , 'department' => $department]
            ]);
        }

    }


    public function appointments_all(Request $request){
            $doctors = Doctor::all();
            $hospitals = Hospital::all();
            $departments = Department::all();
            $shifts = Doctor_Schedule::all();
        
            $schedules = array();

            foreach($shifts as $shift){
                if(!empty($shift->from) ){
                    $from = Carbon::parse($shift->from)->format('H:i:s');
                    $to = Carbon::parse($shift->to)->format('H:i:s');
                   
                    $current_date_time = Carbon::now();
 
                    $current_time = $current_date_time->format('H:i:s');
                    $current_date = $current_date_time->format('Y-m-d');

                   

                    if(!empty($from) && !empty($to)){
                        
                        if($shift->date >= $current_date ){
                           array_push($schedules , $shift);
                        }
                    }
                        
                    
                }
                
            }

            // && $from >= $current_time

            if($request->wantsJson()){
                return response()->json([
                    'status' => 'success',
                    'message' => 'Details list',
                    'data' => ['doctors' => $doctors , 'hospitals'=> $hospitals , 'departments' => $departments,'schedules'=>$schedules]
                ]);
            }
    }

}

