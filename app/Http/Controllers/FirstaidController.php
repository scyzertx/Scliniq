<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Firstaid;
use App\FirstAidDescription;

class FirstaidController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //return firstaid with respective descriptions
        $firstaids = Firstaid::with('details')->get();        

        if($request->wantsJson()){
            return response()->json([
                'status' => 'success',
                'message' => 'Firstaid list',
                'data' => $firstaids
            ]); 
        }

        return view('firstaids.index' ,compact('firstaids'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('firstaids.create');
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $add_new=Firstaid::create($request->all());

        foreach($request['descriptions'] as $single_description){
            $new_description = new FirstAidDescription;
            $new_description->firstaid_id = $add_new->id;
            $new_description->descriptions = $single_description;
            $new_description->save();
        }


        return redirect('/firstaids');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request , $id)
    {
       $firstaid = Firstaid::find($id);

       if($request->wantsJson()){
        return response()->json([
            'status' => 'success',
            'message' => 'respective firstaid details',
            'data' => $firstaid
        ]); 
       }
       return view('firstaids.detail' , compact('firstaid')); 

 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
