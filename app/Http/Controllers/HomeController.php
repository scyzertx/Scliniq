<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Medicine;
use App\Medicine_Schedule;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function notification(){
        $date_time = Carbon::now();

        $time = $date_time->format('H:i:s');
        $date = $date_time->format('Y-m-d');

        $medicines = Medicine::where('start_date' , $date)->get();

        foreach($medicines as $medicine){

            $schedules = $medicine->schedules;
            foreach($schedules as $schedule){
                $explode = explode(':' , $schedule->time);
                $current_explode = explode(':' , $time);
                if($schedule->count == FALSE){
                    if( ($explode[0] == $current_explode[0]) && ($explode[1] == $current_explode[1])){
                   
                        $user = User::where('id' , $medicine->user_id)->first();
                        $obj = Medicine_Schedule::where('time' , $schedule->time)->where('medicine_id' , $schedule->medicine_id)->first();
                        $obj->count = true;
                        $obj->save();
    
                        $f_token = $user->f_token;
                        
                        $this->send_notification($f_token);
    
    
                    }
                }
                
                
            }
        }

    }

    public function send_notification($token)
    {
        $msg = array
        (
            'body' => 'Time to take Medicine',
            'title' => 'Medicine Remainder'
        );
        $fields = array
        (
            'to' => $token,
            'notification' => $msg
        );


        $headers = array
        (
            'Authorization: key=AAAAIGIxPKE:APA91bHvBzSRzXfRKVtuiji5uXhLYn_dEv3qyFqvnIDSygYbIadJkmwcg6WboUJjUOjKZ_H1leHZxbmsGw9zAaUodjgXI9wXZhPZ-b326MTbB537y8RycVGRzHT8Lt4rxZt0ZZqrME1q',
            'Content-Type: application/json'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }
}
