<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Hospital;
use App\Department;
use App\Doctor;

class HospitalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $hospitals = Hospital::all();

        if($request->wantsJson()){
            return response()->json([
                'status' => 'success',
                'message' => 'Hospital list',
                'data' => $hospitals
            ]); 
        }

        return view('hospitals.index' ,compact('hospitals'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('hospitals.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //setting the default image for restaurant if its image is not set
        if(!empty($request->image_file)){
            $getImageName =  time().str_random(4).'.'.$request->image_file->getClientOriginalExtension();
            $request->image_file->move(public_path('images') , $getImageName);
            $request['images'] = $getImageName;
        }else{
            $request['images'] = 'upload_user.png';
        }

        $add_new=Hospital::create($request->all());
        return redirect('/hospitals');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $hospital=Hospital::find($id);
        return view('hospitals.edit', compact('hospital'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $old_hospital=Hospital::find($id);
        $new_hospital=$request->all();
        $old_hospital->fill($new_hospital)->save();
        return redirect ('/hospitals');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $hospital=Hospital::find($id);
        $hospital->delete();
        return redirect('/hospitals');
    }

    public function show_departments(Request $request){

        $id = $request->id;
        
        $hospital = Hospital::findorfail($id);
    
        $options = "<option>Select Departments</option>";
        
        foreach($hospital->departments as $department){
            $options .= "<option value='".$department->id."'>$department->name</option>";
        }

        return $options;


    }

    public function show_doctors(Request $request){

        $id = $request->id;
        
        $options = "<option>Select Doctors</option>";
        
        $doctors = Doctor::where('department_id' , $id)->get();

        foreach($doctors as $doctor){
            $options .= "<option value='".$doctor->id."'>$doctor->name</option>";
        }

        return $options;


    }

    public function show_hospital_departments(Request $request,$id){

        
        $departments = Department::where('hospital_id' , $id)->get();

        if($request->wantsJson()){
            return response()->json([
                'status' => 'success',
                'message' => 'Hospital list',
                'data' =>   $departments,
            ]); 
        }

        return view('departments.index' , compact('departments'));

    }


    public function show_hospital_departments_doctors(Request $request, $id){
        $doctors= Doctor::where('department_id', $id)->get();
        if($request->wantsJson()){
            return response()->json([
                'status' => 'success',
                'message' => 'Hospital list',
                'data' => $doctors
            ]); 
        }
        return view('doctors.index', compact('doctors'));
    }
}
