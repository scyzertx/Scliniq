<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Medicine;
use App\Medicine_Schedule;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\User;

class MedicineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $medicines = Medicine::with('schedules')->get();


        if($request->wantsJson()){
            return response()->json([
                'status' => 'success',
                'message' => 'Medicines list',
                'data' => $medicines
            ]); 
        }

        return view('medicines.index' ,compact('medicines'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('medicines.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $request['start_date']=Carbon::createFromFormat('m/d/Y' , $request->start_date);
        // $request['end_date']=Carbon::createFromFormat('m/d/Y' , $request->end_date);
        
        $medicine = new Medicine();
        $medicine->name = $request->input('name');
        $medicine->user_id=$request['user_id'];
        $medicine->start_date=$request->start_date;
        $medicine->end_date=$request->end_date;    
        $medicine->save();


       
        $schedules = array();
        if(!empty($request['time1'])){
            $schedule = new Medicine_Schedule();
            $schedule->medicine_id = $medicine->id;
            // $new_time = Carbon::createFromFormat('H:i a' , $single_time);
            $schedule->time = $request['time1'];
            $schedule->save();
            array_push($schedules , $schedule);
        }

        if(!empty($request['time2'])){
            $schedule = new Medicine_Schedule();
            $schedule->medicine_id = $medicine->id;
            // $new_time = Carbon::createFromFormat('H:i a' , $single_time);
            $schedule->time = $request['time2'];
            $schedule->save();
            array_push($schedules , $schedule);
        }
        if(!empty($request['time3'])){
            $schedule = new Medicine_Schedule();
            $schedule->medicine_id = $medicine->id;
            // $new_time = Carbon::createFromFormat('H:i a' , $single_time);
            $schedule->time = $request['time3'];
            $schedule->save();
            array_push($schedules , $schedule);
        }

        if(!empty($request['time4'])){
            $schedule = new Medicine_Schedule();
            $schedule->medicine_id = $medicine->id;
            // $new_time = Carbon::createFromFormat('H:i a' , $single_time);
            $schedule->time = $request['time4'];
            $schedule->save();
            array_push($schedules , $schedule);
        }
        // foreach($request['time'] as $single_time){
            

        //     $schedule = new Medicine_Schedule();
        //     $schedule->medicine_id = $medicine->id;
        //     // $new_time = Carbon::createFromFormat('H:i a' , $single_time);
        //     $schedule->time = $single_time;
        //     $schedule->save();
        // }  
        
    
        if($request->wantsJson()){
            return response()->json([
                'status' => 'success',
                'message' => 'Medicine store successfully',
                'data' => [$medicine , $schedules]
            ]); 
        }

        return redirect('/medicines'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $medicine=Medicine::find($id);

        if($request->wantsJson()){
            return response()->json([
                'status' => 'success',
                'message' => 'respective medicines details',
                'data' => $medicine
            ]); 
        }
        return view('medicines.edit', compact('medicine'));
    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $medicine_schedule=Medicine_Schedule::find($id);
        $medicine_schedule->delete();
        return redirect('/medicines');
    
    }

    //users' medicines
    public function user_medicine(Request $request){
        $user = User::findorfail($request['user_id']);

        $medicine = $user->medicines;

        if($request->wantsJson()){
            return response()->json([
                'status' => 'success',
                'message' => 'Users medicines',
                'data' => $medicine
            ]);
        }
    }


    //users' medicines time 
    public function medicine_time(Request $request){

        $medicine = Medicine::findorfail($request['medicine_id']);

        $time = $medicine->schedules;

        if($request->wantsJson()){
            return response()->json([
                'status' => 'success',
                'message' => 'Medicine respective time',
                'data' => $time
            ]);
        }
    }
}
