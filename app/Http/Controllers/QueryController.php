<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Query;
use Auth;
use App\Answer;

class QueryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $queries = Query::all();

        if($request->wantsJson()){
            return response()->json([
                'status' => 'success',
                'message' => 'Question list',
                'data' => $queries
            ]); 
        }
        return view('queries.index' , compact('queries') ); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    
        $query = Query::create($request->all());

        if($request->wantsJson()){
            return response()->json([
                'status' => 'success',
                'message' => 'question stored',
                'data' => $query,
            ]); 
        }

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id , Request $request)
    {
        $query = Query::findorfail($id);
        $answers = $query->answers;

        if($request->wantsJson()){
            return response()->json([
                'status' => 'success',
                'message' => 'return question and its answers',
                'data' => ['question' => $query]
            ]); 
        }

        return view('queries.create' , compact('query' , 'answers'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function store_answer(Request $request)
    {
        
        $answer = Answer::create($request->all());
        
        if($request->wantsJson()){
            return response()->json([
                'status' => 'success',
                'message' => 'answer stored',
                'data' => $answer
            ]); 
        }
        return redirect()->back();
    
    }
}
