<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Hospital;
use App\Doctor_Schedule;
use App\User;
use App\Doctor;
use Auth;
use Carbon\Carbon;

class ScheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        

        return view('doctors.index_schedule');
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $check_array= array();
        $doctors = Doctor::where('name' , Auth::user()->name)->get();
        foreach($doctors as $doctor){
            array_push($check_array , $doctor->hospital_id);
        }

        
        $hospitals = Hospital::whereIn('id' , $check_array)->get();
        return view('doctors.set_schedule' , compact('hospitals'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 
        
        if($request->wantsJson()){
            $doctor = Doctor::find($request['doctor_id']);
            $doctor_info = Doctor::where('name' , $doctor->name)->where('hospital_id' , $request['hospital_id'])->first();
        
        }else{
            $doctor_info = Doctor::where('name' , Auth::user()->name)->where('hospital_id' , $request['hospital_id'])->first();
        }
        

    
        
        $request['date']=$request->date;

        
          
        if($request->wantsJson()){
            $request['start_morning']=!empty($request['start_morning']) ? $request->start_morning : NULL;
            $request['end_morning']=!empty($request['end_morning']) ?  $request->end_morning : NULL;
            $request['start_day']=!empty($request['start_day']) ?  $request->start_day : NULL;
            $request['end_day']=!empty($request['end_day']) ?$request->end_day : NULL;
            $request['start_evening']=!empty($request['start_evening']) ?  $request->start_evening : NULL;
            $request['end_evening']=!empty($request['end_evening']) ? $request->end_evening : NULL;
            
              
        }else{
            $request['start_morning']=!empty($request['start_morning']) ? Carbon::createFromFormat('H:i a' , $request->start_morning) : NULL;
            $request['end_morning']=!empty($request['end_morning']) ? Carbon::createFromFormat('H:i a' , $request->end_morning) : NULL;
            $request['start_day']=!empty($request['start_day']) ? Carbon::createFromFormat('H:i a' , $request->start_day) : NULL;
            $request['end_day']=!empty($request['end_day']) ? Carbon::createFromFormat('H:i a' , $request->end_day) : NULL;
            $request['start_evening']=!empty($request['start_evening']) ? Carbon::createFromFormat('H:i a' , $request->start_evening) : NULL;
            $request['end_evening']=!empty($request['end_evening']) ? Carbon::createFromFormat('H:i a' , $request->end_evening) : NULL;
            
        }
        

    
        $errors = array();

        $set_schedule = Doctor_Schedule::where('date' , $request['date'])->where('doctor_id' , $request['doctor_id'])->where('hospital_id' , $request['hospital_id'])->count();
        if($set_schedule > 0){
            array_push($errors , 'Schedule already set for '.$request['date']);
        }
   

        if( (empty($request['start_morning']) && empty($request['end_morning'])) && (empty($request['start_day']) && empty($request['start_day'])) && (empty($request['start_evening']) && empty($request['end_evening']))){
            array_push($errors , 'at least one shift is required');
        }

        if(!empty($request['start_morning']) && !empty($request['end_morning'])){
            
            if($request['end_morning'] < $request['start_morning']){
                array_push($errors , 'Ending time must be greater in 1st shift');
            }

        }elseif((!empty($request['start_morning']) && empty($request['end_morning'])) || (empty($request['start_morning']) && !empty($request['end_morning']))){
            array_push($errors , 'Both field are required in 1st shift');
        }

       
      
        
        if(!empty($request['start_day']) && !empty($request['start_day'])){
            if($request['end_day'] < $request['start_day']){
                array_push($errors , 'Ending time must be greater in 2nd shift');
            }
        }elseif((!empty($request['start_day']) && empty($request['end_day'])) || (empty($request['start_day']) && !empty($request['end_day']))){
            array_push($errors , 'Both field are required in 2nd shift');
        }
        
      
        if(!empty($request['start_evening']) && !empty($request['end_evening'])){
            if($request['end_evening'] < $request['start_evening']){
                array_push($errors , 'Ending time must be greater in 3rd shift');
            }    
        }elseif((!empty($request['start_evening']) && empty($request['end_evening'])) || (empty($request['start_evening']) && !empty($request['end_evening']))){
            array_push($errors , 'Both field are required in 3rd shift');
        }

        $count = count($errors);

      
        if($count > 0){
            if($request->wantsJson()){
                return response()->json([
                    'status' => 'fail',
                    'message' => 'Validation fails',
                    'data' => $errors
    
                ]);
    
            }

            return redirect()->back();
        }
        
           
        
        if(!empty($doctor_info)){
            
            //morning shift
            $new_data = new Doctor_Schedule;
            $new_data->doctor_id = $doctor_info->id;
            $new_data->hospital_id = $request['hospital_id'];
            $new_data->department_id = $doctor_info['department_id'];
            $new_data->date = $request['date'];
            $new_data->from = $request['start_morning'];
            $new_data->to = $request['end_morning'];
            $new_data->save();

            //day shift
            $new_data = new Doctor_Schedule;
            $new_data->doctor_id = $doctor_info->id;
            $new_data->hospital_id = $request['hospital_id'];
            $new_data->department_id =$doctor_info['department_id'];
            $new_data->date = $request['date'];
            $new_data->from = $request['start_day'];
            $new_data->to = $request['end_day'];
            $new_data->save();


            //night shift
            $new_data = new Doctor_Schedule;
            $new_data->doctor_id = $doctor_info->id;
            $new_data->hospital_id = $request['hospital_id'];
            $new_data->department_id =$doctor_info['department_id'];
            $new_data->date = $request['date'];
            $new_data->from = $request['start_evening'];
            $new_data->to = $request['end_evening'];
            $new_data->save();

            if($request->wantsJson()){
                return response()->json([
                    'status' => 'success',
                    'message' => 'Appointment added',
                    
                ]);
    
            }

            return redirect('/schedule');
        }else{
            if($request->wantsJson()){
                return response()->json([
                    'status' => 'fail',
                    'message' => 'Appointment fails',
                    
                ]);
    
            }

            return redirect()->back();
            
         }
        
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id , Request $request)
    {
        $single_schedule = Doctor_Schedule::find($id);
        
        $many_schedules = Doctor_Schedule::where('created_at' , $single_schedule->created_at)->get();
        if($many_schedules->count() > 1){
            $schedules = $many_schedules;
        }else{
            $schedules = $single_schedule;
        }

        if($request->wantsJson()){
            return response()->json([
                'status' => 'success',
                'message' => 'Schedules details',
                'data' => $schedules
            ]);
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function show_schedule(Request $request)
    {
        $shifts = Doctor_Schedule::where('doctor_id' , $request['doctor_id'])
                                ->where('hospital_id' , $request['hospital_id'])
                                ->where('date' , $request['date'])
                                ->get();

        $options = "<option>Select Shifts</option>";
        

        foreach($shifts as $shift){
            if(!empty($shift->from) ){
                $from = Carbon::parse($shift->from)->format('H:i:s');
                $to = Carbon::parse($shift->to)->format('H:i:s');
                $current_date_time = Carbon::now();
                
                $current_time = $current_date_time->format('H:i:s');
                if(!empty($from) && !empty($to)){
                    if($current_time >= $from && $current_time <= $to){    
                        $options .= "<option value='".$shift->from.' '.$shift->to."'> $from to $to  </option>";
                    }
                }
                
                
                
            }
            
        }

        return $options;                        
    }

    //doctor kun kun hospital ma xa 
    public function doctor_hospitals(Request $request){
        $check_array= array();
        $specific_doctor = User::find($request['doctor_id']);
        $doctors = Doctor::where('name' , $specific_doctor['name'] )->get();
        foreach($doctors as $doctor){
            array_push($check_array , $doctor->hospital_id);
        }

        
        $hospitals = Hospital::whereIn('id' , $check_array)->get();
        if($request->wantsJson()){
            return response()->json([
                'status' => 'success',
                'message' => 'Doctors hospitals details' ,
                'data' => $hospitals
            ]);
        }

        // return view('doctors.set_schedule' , compact('hospitals'));

    }

    //doctor ko kun hospital ma k schedule xa 
    public function doctor_schedule_details(Request $request)
    {
   
        $schedules = Doctor_Schedule::where('doctor_id' , $request['doctor_id'])->get()->groupBy('date');
       
        if($request->wantsJson()){
            return response()->json([
                'status' => 'success',
                'message' => 'Schedules details',
                'data' => $schedules
            ]);
        }
    }
}
