<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Mail\VerifyDoctor;
use Illuminate\Support\Facades\Mail;
use App\Hospital;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(Auth::user()->admin == 2){
            $users = User::where('id' , Auth::user()->id)->get();
        }else
        {
            $users = User::with('user_detail')->get();  
        }

    
            if($request->wantsJson())
            {
                return response()->json([
                    "status"=> 'success',
                    "message"=> 'All Users List',
                    "data"=> $users
                ]);
            }
            return view('users.index', compact('users'));

   
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $hospitals = Hospital::all();
        return view('users.create' , compact('hospitals'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       if($request['admin'] == 3){
        $raw_password=str_random(9);
        $request['password']= bcrypt($raw_password);
       }else{
        $request['password']=bcrypt($request->password);
       }

       $request['verified']= true;
        
        $add_new=User::create($request->all());

        $add_new['raw_password']= $raw_password;

        Mail::to($add_new->email)->send(new VerifyDoctor($add_new));

        return redirect('/users'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
            $hospitals=Hospital::all();
            $user= User::find($id);
            return view('users.edit' , compact('user', 'hospitals'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $old_user=User::find($id);
        $new_user=$request->all();
        $old_user->fill($new_user)->save();
        return redirect ('/users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user=User::find($id);
        $user->delete();
        return redirect('/users');
    }
}
