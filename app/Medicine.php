<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Medicine extends Model
{
    protected $fillable=['name'];

    public function schedules(){
        return $this->hasMany('App\Medicine_Schedule');
    }
}
