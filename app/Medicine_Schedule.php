<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Medicine_Schedule extends Model
{
    protected $table="medicine_schedules";
    protected $fillable=['medicine_id','user_id', 'time','start_date','end_date'];

    public function medicine(){
        return $this->belongsTo('App\Medicine');
    }
}
            