<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Query extends Model
{
    protected $fillable=['queries' , 'user_id'];

    public function answers(){
        return $this->hasMany('App\Answer');
    }
}
