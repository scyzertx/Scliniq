<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;


class User extends Authenticatable
{
    use HasApiTokens, Notifiable;
    

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'id', 'name', 'email', 'password', 'phone_number' , 'admin' , 'hospital_id','verified' ,'f_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function hospital(){
        return $this->belongsTo('App\Hospital');
    }
    
    public function verify_user()
    {
        return $this->hasOne('App\VerifyUser');
    }

    public function user_detail(){
        return $this->hasOne('App\User_Detail');
    }

    public function medicines(){
        return $this->hasMany('App\Medicine');
    }

   
}
