<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User_Detail extends Model
{

    protected $table = 'user_details';
    protected $fillable=['address','age', 'gender','blood_group','height', 'weight', 'marital_status'];

    public function user(){
        return $this->belongsTo('App/User');
    }
}
