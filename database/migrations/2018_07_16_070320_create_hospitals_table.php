<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHospitalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hospitals', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('address');
            $table->string('images');
            $table->string('latitude');
            $table->string('longitude');
            $table->string('contact_no');
            $table->longtext('description');
            $table->timestamps();
        });

        Schema::table('users' , function($table){
            $table->foreign('hospital_id')->references('id')->on('hospitals')->onDelete('cascade')->onUpdate('cascade');
        });

        DB::table('users')->insert([
            'name' => 'admin',
            'password' => bcrypt('password'),
            'email' => 'superadmin@admin.com',
            'phone_number' => '981718676',
            'admin'=>1,
           
        ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hospitals');
    }
}
