<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('answers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('doctor_name');
            $table->unsignedInteger('query_id');
            $table->foreign('query_id')->references('id')->on('queries')->onDelete('cascade')->onUpdate('cascade');
            $table->longtext('answers');
            $table->timestamps();
        });
        

        DB::table('oauth_clients')->insert([
            [
                'name' => 'Laravel Personal Access Client',
                'secret' => 'TM4of1XI3ijdHUgOTIxlcFiARBCgm3oCmc8BEAmu',
                'redirect' => 'http://localhost',
                'personal_access_client' => 1,
                'password_client' => 0,
                'revoked' => 0,
            ],
            [
                'name' => 'Laravel Password Grant Client',
                'secret' => 'mf1G7bKyDZ2erA4sKaTzRDHvrawved6GP2LzJljJ',
                'redirect' => 'http://localhost',
                'personal_access_client' => 0,
                'password_client' => 1,
                'revoked' => 0,
            ],
            [
                'name' => 'scliniq',
                'secret' => 'sPwGfTX2FeaaEyl7wNg2PlGFhuLsSvtkUddIy2yL',
                'redirect' => 'http://localhost',
                'personal_access_client' => 0,
                'password_client' => 1,
                'revoked' => 0,
            ]
        ]);






    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeginKeyConstraints();
        Schema::dropIfExists('answers');
    }
}
