<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFirstaidDescriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('firstaid_descriptions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('firstaid_id');
            $table->foreign('firstaid_id')->references('id')->on('firstaids')->onDelete('cascade')->onUpdate('cascade');
            $table->longtext('descriptions');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('firstaid_descriptions');
    }
}
