@extends('layouts.admin_layout')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Make Appointment</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="/appointments">
                        {{ csrf_field() }}

                        
                        <div class="form-group{{ $errors->has('hospital_id') ? ' has-error' : '' }}">
                            <label for="hospital_id" class="col-md-4 control-label">Hospital Name</label>

                            <div class="col-md-6">
                                    <select name="hospital_id" id="select-hospital" class="form-control">
                                        @foreach($hospitals as $hospital)
                                            <option value="{{$hospital->id}}" @if($selected_hospital->id == $hospital->id) selected @endif>{{$hospital->name}}</option>
                                        @endforeach
                                    </select>
                                @if ($errors->has('hospital_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('hospital_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        
                        <div class="form-group{{ $errors->has('department_id') ? ' has-error' : '' }}">
                            <label for="department_id" class="col-md-4 control-label">Department</label>

                            <div class="col-md-6">
                                    <select name="department_id" id="select-department" class="form-control">
                                        @foreach($departments as $department)
                                            <option value="{{$department->id}}" @if($selected_department->id == $department->id) selected @endif> {{$department->name}}</option>
                                        @endforeach
                                    </select>
                                @if ($errors->has('department_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('department_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>



                        <div class="form-group{{ $errors->has('doctor_id') ? ' has-error' : '' }}">
                            <label for="doctor_id" class="col-md-4 control-label">Doctor Name</label>

                            <div class="col-md-6">
                                 <select name="doctor_id" id="select-doctor" class="form-control">
                                 @foreach($doctors as $doctor)
                                         <option value="{{$doctor->id}}" @if($selected_doctor->id == $doctor->id) selected @endif>{{$doctor->name}}</option>
                                 @endforeach
                                 </select>
                                @if ($errors->has('doctor_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('doctor_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('date') ? ' has-error' : '' }}">
                            <label for="date" class="col-md-4 control-label">Date</label>

                            <div class="col-md-6">
                                <input id="date" type="text" class="form-control datepicker"  name="date" value="{{ old('date') }}" required autocomplete='off'>

                                @if ($errors->has('date'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('date') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div id="schedule-view">
                            

                            <div class="form-group{{ $errors->has('doctor_id') ? ' has-error' : '' }}">
                                <label for="doctor_id" class="col-md-4 control-label">Choose Shift</label>

                            <div class="col-md-6" >
                                
                            <select name="shifts" id="select-shifts" class="form-control">
                                 
                            </select>

                                
                            </div>
                        </div>
                            
                        </div>



                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Book Appointment
                                </button>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $('#select-hospital').change(function(){
        var id = $(this).val();

        <?php $url = url('/hospital/departments')?>

         $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                    'Accept': 'application/json'
                },
                type: "GET",
                data :'id='+id,
                url:"<?= $url?>",

                accepts: {
                    text: "application/json"
                },
                success: function (result) {
                    document.getElementById('select-department').innerHTML = result;
                    document.getElementById('select-doctor').innerHTML = "<option>Select Doctors</option>";
                },
                error: function (e) {
                    alert("Error Occured!");
                    console.log("ERROR: ", e)
                }
            });

        

    })


      $('#select-department').change(function(){
        var id = $(this).val();

           <?php $next_url = url('/hospital/doctors')?>

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                    'Accept': 'application/json'
                },
                type: "GET",
                data :'id='+id,
                url:"<?= $next_url?>",

                accepts: {
                    text: "application/json"
                },
                success: function (result) {
                    console.log(result);
                    document.getElementById('select-doctor').innerHTML = result;
                },
                error: function (e) {
                    alert("Error Occured!");
                    console.log("ERROR: ", e)
                }
            });

    })

    $('.datepicker').change(function(){
        var date = $(this).val();

           <?php $next_url = url('/shifts')?>

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                    'Accept': 'application/json'
                },
                type: "POST",
                data :{date : date , doctor_id : $('#select-doctor').val() , hospital_id : $('#select-hospital').val()},
                url:"<?= $next_url?>",

                accepts: {
                    text: "application/json"
                },
                success: function (result) {
                    console.log(result);
                    $('#select-shifts').html(result);
                    
                    
                },
                error: function (e) {
                    alert("Error Occured!");
                    console.log("ERROR: ", e)
                }
            });
    })

   

     


</script>    
@endsection