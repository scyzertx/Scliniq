@extends('layouts.admin_layout')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Make Appointment</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="/appointments/{{$appointment->id}}">
                        {{ csrf_field() }}
                        {{method_field('PUT')}}

                        <div class="form-group{{ $errors->has('hospital_id') ? ' has-error' : '' }}">
                            <label for="hospital_id" class="col-md-4 control-label">Hospital Name</label>

                            <div class="col-md-6">
                                    <select name="hosptal_id" id="" class="form-control">
                                        @foreach($hospitals as $hospital)
                                            <option value="{{$hospital->id}}">{{$hospital->name}}</option>
                                        @endforeach
                                    </select>
                                @if ($errors->has('hospital_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('hospital_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('department_id') ? ' has-error' : '' }}">
                            <label for="department_id" class="col-md-4 control-label">Department</label>

                            <div class="col-md-6">
                                    <select name="department_id" id="" class="form-control">
                                        @foreach($departments as $department)
                                            <option value="{{$department->id}}">{{$department->name}}</option>
                                        @endforeach
                                    </select>
                                @if ($errors->has('department_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('department_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>



                        <div class="form-group{{ $errors->has('doctor_id') ? ' has-error' : '' }}">
                            <label for="doctor_id" class="col-md-4 control-label">Doctor Name</label>

                            <div class="col-md-6">
                                 <select name="doctor_id" id="" class="form-control">
                                 @foreach($doctors as $doctor)
                                         <option value="{{$doctor->id}}" @if($doctor->id == $appointment->doctor_id) 'Selected' @endif>{{$doctor->name}}</option>
                                 @endforeach
                                 </select>
                                @if ($errors->has('doctor_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('doctor_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('time') ? ' has-error' : '' }}">
                            <label for="time" class="col-md-4 control-label">Time</label>

                            <div class="col-md-6 bootstrap-timepicker" >
                                <input id="time" type="text" class="form-control timepicker" name="time" value="{{$appointment->time}}" required >

                                @if ($errors->has(''))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('time') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('date') ? ' has-error' : '' }}">
                            <label for="date" class="col-md-4 control-label">Date</label>

                            <div class="col-md-6">
                                <input id="date" type="text" class="form-control datepicker" name="date" value="{{$appointment->date}}" required autofocus>

                                @if ($errors->has('date'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('date') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Book Appointment
                                </button>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
