@extends('layouts.admin_layout')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Appointment
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Appointment</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="box">
                    <div class="box-header">
                        
                        <small style="margin-left: 15px">
                        @if(Auth::user()->admin==0)
                            <a href={{route('appointments.create')  }}>
                                <button type="submit" class="btn btn-success">Make Appointment</button>
                            </a>
                            @endif
                        </small>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive">
                       
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>S.N.</th>
                                <th>Hospital Name</th>
                                <th>Deparment</th>
                                <th>Doctors name</th>
                                <th>From</th>
                                <th>To</th>
                                <th>Date</th>
                           </tr>
                            </thead>
                            <tbody>

                           @php($i=1)
                                @foreach($appointments as $appointment)
                                    <tr>
                                        <td>{{$i}}</td>
                                        <td>{{$appointment->hospital->name}}</td>
                                        <td>{{$appointment->department->name}}</td>
                                        <td>{{$appointment->doctor->name}}</td>
                                        <td>{{$appointment->from}}</td>
                                        <td>{{$appointment->to}}</td>
                                        <td>{{$appointment->date}}</td>
                                       
                                        <td>
                                            <div class="btn-group">
                                               @if(Auth::user()->admin==0)
                                                <a href={{route('appointments.edit' ,  ['appointment' =>$appointment->id])  }}>
                                                    <button type="submit" class="btn btn-primary">Edit</button>
                                                </a>
                                                <form action={{route('appointments.destroy' ,['appointment'=>$appointment->id])}} method="POST"
                                                      style="display: inline">
                                                    {{csrf_field()}}
                                                    {{method_field("DELETE")}}
                                                    <button type="submit" class="btn btn-danger"
                                                            onclick="return confirm('Are you sure you want to delete?')">
                                                          
                                                        Delete
                                                    </button>
                                                </form>
                                            @endif
                                            </div>
                                        </td>
                                    </tr>
                                    @php($i++)
                                @endforeach

                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>

        </div>
    </section>
    <!-- /.content -->

@endsection
