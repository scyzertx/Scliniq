@extends('layouts.admin_layout')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Departments
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Departments</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Departments</h3>
                        <small style="margin-left: 15px">
                        @if(Auth::user()->admin==1 || Auth::user()->admin==2)
                            <a href={{route('departments.create')  }}>
                                <button type="submit" class="btn btn-success">Add New</button>
                            </a>
                            @endif
                        </small>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive">
                       
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>S.N.</th>
                                <th>Hospital</th>
                                <th>Deparment</th>
                           </tr>
                            </thead>
                            <tbody>

                           @php($i=1)
                                @foreach($departments as $department)
                                    <tr>
                                        <td>{{$i}}</td>
                                        <td>{{$department->hospital->name}}</td>  
                                        <td>{{$department->name}}</td>
                                                                  
                                        <td>
                                        @if(Auth::user()->admin==0)
                                            <div class="btn-group">
                                            <a href={{url('hospital/departments/doctors/'.$department->id)  }}>
                                                    <button type="submit" class="btn btn-primary">Doctors</button>
                                                </a>
                                                @endif
                                               @if(Auth::user()->admin==1 || Auth::user()->admin==2)
                                                <a href={{route('departments.edit' ,  ['department' =>$department->id])  }}>
                                                    <button type="submit" class="btn btn-primary">Edit</button>
                                                </a>
                                                <form action={{route('departments.destroy' ,['department'=>$department->id])}} method="POST"
                                                      style="display: inline">
                                                    {{csrf_field()}}
                                                    {{method_field("DELETE")}}
                                                    <button type="submit" class="btn btn-danger"
                                                            onclick="return confirm('Are you sure you want to delete?')">
                                                          
                                                        Delete
                                                    </button>
                                                </form>
                                            
                                            </div>
                                            @endif
                                        </td>
                                    </tr>
                                    @php($i++)
                                @endforeach

                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>

        </div>
    </section>
    <!-- /.content -->

@endsection
