@extends('layouts.admin_layout')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Doctors
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Schedule  List</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Doctors List</h3>
                        <small style="margin-left: 15px">
                        @if(Auth::user()->admin==3)
                            <a href={{route('schedule.create')  }}>
                                <button type="submit" class="btn btn-success">Add New</button>
                            </a>
                            @endif
                        </small>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive">
                       
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>S.N.</th>
                                <th>Hospitals name</th>
                                <th>Date</th>
                                <th colspan="2">Morning Shift</th>
                                <th colspan="2">Day Shift</th>
                                <th colspan="2">Evening Shift</th>
                           </tr>
                            </thead>
                            <tbody>

                           @php($i=1)
                               
                                    <tr>
                                        <td>{{$i}}</td>
                                        <td>haha</td>
                                        <td>hehe</td>
                                        <th>From</th>
                                        <th>To</th>
                                        <th>From</th>
                                        <th>To</th>
                                        <th>From</th>
                                        <th>To</th>
                                        <td>
                                        <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>aw</td>
                                        <td>aws</td>
                                        </tr>
                                            <!-- <div class="btn-group">
                                               @if (Auth::user()->admin==1 || Auth::user()->admin==2)
                                                <a href={{route('doctors.edit' ,  ['doctor' =>$doctor->id])  }}>
                                                    <button type="submit" class="btn btn-primary">Edit</button>
                                                </a>
                                                <form action={{route('doctors.destroy' ,['doctor'=>$doctor->id])}} method="POST"
                                                      style="display: inline">
                                                    {{csrf_field()}}
                                                    {{method_field("DELETE")}}
                                                    <button type="submit" class="btn btn-danger"
                                                            onclick="return confirm('Are you sure you want to delete?')">
                                                          
                                                        Delete
                                                    </button>
                                                </form>
                                            @endif
                                            </div> -->

                                              <!-- <div class="btn-group"> -->
                                               @if ( Auth::user()->admin==0)
                                                 <a href={{url('/hospital/doctors/appointment/'.$doctor->id) }}>
                                                   <button type="submit" class="btn btn-success">Make Appointment</button>
                                                 </a>
                                               
                                               @endif
                                               </div>
                                        </td>
                                    </tr>
                                   
                                    @php($i++)
                               
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>

        </div>
    </section>
    <!-- /.content -->

@endsection
