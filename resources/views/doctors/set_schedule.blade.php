@extends('layouts.admin_layout')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Set Appointment</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="/schedule">
                        {{ csrf_field() }}

                        
                        <div class="form-group{{ $errors->has('hospital_id') ? ' has-error' : '' }}">
                            <label for="hospital_id" class="col-md-4 control-label">Hospital Name</label>

                            <div class="col-md-6">
                                    <select name="hospital_id" id="select-hospital" class="form-control">
                                        @foreach($hospitals as $hospital)
                                            <option value="{{$hospital->id}}">{{$hospital->name}}</option>
                                        @endforeach
                                    </select>
                                @if ($errors->has('hospital_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('hospital_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        
                        <div class="form-group{{ $errors->has('date') ? ' has-error' : '' }}">
                            <label for="date" class="col-md-4 control-label">Date</label>

                            <div class="col-md-6">
                                <input id="date" type="text" class="form-control datepicker" name="date" value="{{ old('date') }}"  autocomplete='off'>

                                @if ($errors->has('date'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('date') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        
                        

                        <label>Morning Shift</label>
                        <div class="form-group{{ $errors->has('time') ? ' has-error' : '' }}">
                            <label for="time" class="col-md-4 control-label">From</label>

                            <div class="col-md-6 bootstrap-timepicker">
                                <input  type="text" class="form-control timepicker" name="start_morning" value="{{ old('time') }}"  autocomplete='off' >
                            
                                @if ($errors->has(''))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('time') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('time') ? ' has-error' : '' }}">
                            <label for="time" class="col-md-4 control-label">To</label>

                            <div class="col-md-6 bootstrap-timepicker">
                                <input  type="text" class="form-control timepicker" name="end_morning" value="{{ old('time') }}"  autocomplete='off' >
                            
                                @if ($errors->has(''))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('time') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <label>Day Shift</label>
                        <div class="form-group{{ $errors->has('time') ? ' has-error' : '' }}">
                            <label for="time" class="col-md-4 control-label">From</label>

                            <div class="col-md-6 bootstrap-timepicker">
                                <input  type="text" class="form-control timepicker" name="start_day" value="{{ old('time') }}"  autocomplete='off' >
                            
                                @if ($errors->has(''))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('time') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('time') ? ' has-error' : '' }}">
                            <label for="time" class="col-md-4 control-label">To</label>

                            <div class="col-md-6 bootstrap-timepicker">
                                <input  type="text" class="form-control timepicker" name="end_day" value="{{ old('time') }}"  autocomplete='off' >
                            
                                @if ($errors->has(''))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('time') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <label>Evening Shift</label>
                        <div class="form-group{{ $errors->has('time') ? ' has-error' : '' }}">
                            <label for="time" class="col-md-4 control-label">From</label>

                            <div class="col-md-6 bootstrap-timepicker">
                                <input  type="text" class="form-control timepicker" name="start_evening" value="{{ old('time') }}"  autocomplete='off' >
                            
                                @if ($errors->has(''))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('time') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('time') ? ' has-error' : '' }}">
                            <label for="time" class="col-md-4 control-label">To</label>

                            <div class="col-md-6 bootstrap-timepicker">
                                <input  type="text" class="form-control timepicker" name="end_evening" value="{{ old('time') }}"  autocomplete='off' >
                            
                                @if ($errors->has(''))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('time') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Set Appointment
                                </button>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
