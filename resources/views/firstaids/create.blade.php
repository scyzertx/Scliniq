@extends('layouts.admin_layout')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Add New</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="/firstaids">
                        {{ csrf_field() }}

                    

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">First Aid</label>

                            <div class="col-md-6">
                                <input id="name" type="name" class="form-control" name="name" value="{{ old('name') }}" required>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        
                        

                         <div class="form-group{{ $errors->has('descriptions') ? ' has-error' : '' }}">
                            <label for="details" class="col-md-4 control-label">Details</label>

                            <div class="col-md-6" id='details'>
                                <input id="descriptions" type="text" style="margin-bottom:10px" class="form-control" name="descriptions[]" value="{{ old('details') }}" required autofocus>
                                
                                @if ($errors->has('descriptions'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('descriptions') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="col-md-2">
                                <button type="button"  class="form-control btn btn-success btn-sm" id="add" >Add</button>
                                <button type="button" class='form-control btn btn-danger btn-sm' id="remove" >Remove</button>
                            </div>
                        </div>

                        
                            

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Save
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script>
        $('body').on('click' , '#add' , function(){
            $("#details").append('<input id="descriptions" type="text" style="margin-bottom:10px" class="form-control" name="descriptions[]" >');
        });

        $('body').on('click' , '#remove' , function(){
            var count = $('#details input').length;
            if(count > 1 ){
                $('#details input:last-child').remove();
            }
        });
    </script>
@endsection
