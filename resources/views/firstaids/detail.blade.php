@extends('layouts.admin_layout')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
        {{$firstaid->name}}
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="box">
                    
                    <!-- /.box-header -->
                    <div class="box-body table-responsive">
                       
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>
                                {{$firstaid->name}}
                                </th>
                           </tr>
                           <tr>
                            <th>Details</th>
                           </tr>
                            </thead>
                            <tbody>

                        
                              
                                     
                            @foreach($firstaid->details as $single_details)
                                     <tr>
                                        <td>{{$single_details->descriptions}}</td>
                                    </tr>
                            @endforeach        
                                   
                             

                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>

        </div>
    </section>
    <!-- /.content -->

@endsection
