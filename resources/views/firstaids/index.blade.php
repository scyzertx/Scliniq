@extends('layouts.admin_layout')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            First Aid
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">First Aid</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="box">
                    <div class="box-header">
                        
                        <small style="margin-left: 15px">
                        @if(Auth::user()->admin==1)
                            <a href={{route('firstaids.create')  }}>
                                <button type="submit" class="btn btn-success">Add Firstaid</button>
                            </a>
                           @endif 
                        </small>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive">
                       
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>S.N.</th>
                                <th>First Aids</th>
                                <th>Details</th>
                           </tr>
                            </thead>
                            <tbody>

                           @php($i=1)
                                @foreach($firstaids as $firstaid)
                                    <tr>
                                        <td>{{$i}}</td>                                       
                                        <td>{{$firstaid->name}}</td>
                                        <td><div class="btn-group"><a href={{route('firstaids.show', ['firstaid' => $firstaid->id])}}>
                                                    <button type="submit" class="btn btn-primary">View Details</button>
                                                </a>
                                                </div>
                                                </td>
                                                
                             
                
                                    </tr>
                                    @php($i++)
                                @endforeach

                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>

        </div>
    </section>
    <!-- /.content -->

@endsection
