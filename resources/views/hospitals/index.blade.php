@extends('layouts.admin_layout')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            hospitals
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Hospital List</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Hospitals List</h3>
                        <small style="margin-left: 15px">
                            <a href={{route('hospitals.create')  }}>
                            @if(Auth::user()->admin == 1 )
                                <button type="submit" class="btn btn-success">Add New</button>
                            @endif 
                            </a>
                        </small>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive">
                       
                        <table id="example2" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>S.N.</th>
                                <th>hospitalname</th>
                                <th>Address</th>
                                <th>Image</th>
                                <th>Contact Number</th>
                                <th>Description</th>
                                

                               @if(Auth::user()->admin==1)
                                <th >Action</th>
                               @endif
                             </tr>
                            </thead>
                            <tbody>

                           @php($i=1)
                                @foreach($hospitals as $hospital)
                                    <tr>
                                        <td>{{$i}}</td>
                                        <td>{{$hospital->name}}</td>
                                        <td>{{$hospital->address}}</td>
                                        <td>{{$hospital->image}}</td>
                                        <td>{{$hospital->contact_no}}</td>
                                        <td>{{$hospital->description}}</td>
                                      
                                             <td>                                    
                                            <div class="btn-group">
                                            <a href={{url('/hospital/departments/'.$hospital->id)}}>
                                                    <button type="submit" class="btn btn-primary">Department</button>
                                                </a>
                                              @if(Auth::user()->admin == 1 )
                                                <a href={{route('hospitals.edit' ,  ['hospital' =>$hospital->id])  }}>
                                                    <button type="submit" class="btn btn-primary">Edit</button>
                                                </a>
                                                <form action={{route('hospitals.destroy' ,['hospital'=>$hospital->id])}} method="POST"
                                                      style="display: inline">
                                                    {{csrf_field()}}
                                                    {{method_field("DELETE")}}
                                                    <button type="submit" class="btn btn-danger"
                                                            onclick="return confirm('Are you sure you want to delete?')">
                                                          
                                                        Delete
                                                    </button>
                                                </form>

                                                @endif

                                            </div>
                                        </td>
                                    </tr>
                                    @php($i++)
                                @endforeach

                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>

        </div>
    </section>
    <!-- /.content -->

@endsection
