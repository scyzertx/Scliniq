
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | Starter</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <link rel="stylesheet" href={{asset("bower_components/bootstrap/dist/css/bootstrap.min.css")}}>
    <!-- Font Awesome -->
    <link rel="stylesheet" href={{asset("bower_components/font-awesome/css/font-awesome.min.css")}}>
    <!-- Ionicons -->
    <link rel="stylesheet" href={{asset("bower_components/Ionicons/css/ionicons.min.css")}}>
    <!-- Theme style -->
    <link rel="stylesheet" href={{asset("dist/css/AdminLTE.min.css")}}>
    <!-- Custom Css -->
    <link rel="stylesheet" href={{asset("css/custom.css")}}>

    <!-- DataTables -->
    <link rel="stylesheet" href={{asset("bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css")}}>

    <!-- Select2 -->
    <link rel="stylesheet" href="{{asset('bower_components/select2/dist/css/select2.min.css')}}">

     <!-- bootstrap datepicker -->
  <link rel="stylesheet" href=asset{{("bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css")}}>

<!-- Bootstrap time Picker -->
<link rel="stylesheet" href={{asset("plugins/timepicker/bootstrap-timepicker.min.css")}}>






  <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
        page. However, you can choose any other skin. Make sure you
        apply the skin class to the body tag so the changes take effect. -->
        <link rel="stylesheet" href={{asset("dist/css/skins/skin-blue.min.css")}}>

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

        @yield('styles')
</head>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <!-- Main Header -->
  <header class="main-header">

    <!-- Logo -->
    <a href="index2.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>S</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Scliniq</span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
         
          <!-- /.messages-menu -->

          <!-- Notifications Menu -->
          <li class="dropdown notifications-menu">
            <!-- Menu toggle button -->
      
          <!-- Tasks Menu -->
         
          <!-- User Account Menu -->
          <li class="dropdown user user-menu">
            <!-- Menu Toggle Button -->
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <!-- The user image in the navbar-->
              <img src="dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
              <!-- hidden-xs hides the username on small devices so only the image appears. -->
              <span class="hidden-xs">{{Auth::user()->name}}</span>
            </a>
            <ul class="dropdown-menu">
              <!-- The user image in the menu -->
              <li class="user-header">
                <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">

                <p>
                {{Auth::user()->name}}
                 
                </p>
              </li>
              <!-- Menu Body -->
              <li class="user-body">
                
                <!-- /.row -->
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                              <div class="pull-right">
                                    <a href="{{ route('logout') }}"
                                       class="btn btn-default btn-flat"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        Logout
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </div>

              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
         
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- Sidebar user panel (optional) -->
    

      <!-- search form (Optional) -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
              <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
              </button>
            </span>
        </div>
      </form>
      <!-- /.search form -->

      <!-- Sidebar Menu -->
      <ul class="sidebar-menu" data-widget="tree">
                <li class="header"></li>
                <!-- Optionally, you can add icons to the links -->
                <li class="{{ url()->current() == url('/')? 'active' : '' }}"><a href="{{url('/')}}"><i class="fa fa-link"></i> <span>Home</span></a></li>
                @if(Auth::user()->admin==1)
                <li class="{{ url()->current() == url('/users')? 'active' : '' }} "><a href="{{url('/users')}}"><i class="fa fa-link"></i> <span>Users</span></a></li>

                
                 @endif
                 @if(Auth::user()->admin==1 || Auth::user()->admin==0)
                <li class="{{ url()->current() == url('/hospitals')? 'active' : '' }}"><a href="{{ url('/hospitals') }}"><i class="fa fa-link"></i> <span>Hospitals</span></a></li>
                 @endif

                  
                <li class="{{ url()->current() == url('/firstaids')? 'active' : '' }}"><a href="{{ url('/firstaids') }}"><i class="fa fa-link"></i> <span>First Aid</span></a></li>
                
                 @if( Auth::user()->admin==2)
                <li class="{{ url()->current() == url('/departments')? 'active' : '' }}">
                <a href="{{ url('/departments') }}"><i class="fa fa-link"></i> <span>Departments</span></a></li>
                 @endif
                 @if(Auth::user()->admin==0)
                 <li class="{{ url()->current() == url('/medicines')? 'active' : '' }}">
                <a href="{{ url('/medicines') }}"><i class="fa fa-link"></i> <span>Medicine</span></a></li>
                 @endif
                
                @if(Auth::user()->admin==3)
                <li class="{{ url()->current() == url('/schedule')? 'active' : '' }}">
                <a href="{{ url('/schedule') }}"><i class="fa fa-link"></i> <span>Schedule</span></a></li>
                @endif
                
                <li class="{{ url()->current() == url('/appointments')? 'active' : '' }}">
                    <a href= "{{ url('/appointments') }}"  ><i class="fa fa-link"></i> <span>Appointment</span></a></li>
                    @if(Auth::user()->admin==2)
                <li><a href= "{{ url('/doctors') }}"><i class="fa fa-link"></i> <span>Doctors</span></a></li>
                @endif

                {{--<li class="treeview">--}}
                    {{--<a href="#"><i class="fa fa-link"></i> <span>Multilevel</span>--}}
                        {{--<span class="pull-right-container">--}}
                {{--<i class="fa fa-angle-left pull-right"></i>--}}
              {{--</span>--}}
                    {{--</a>--}}
                    {{--<ul class="treeview-menu">--}}
                        {{--<li><a href="#">Link in level 2</a></li>--}}
                        {{--<li><a href="#">Link in level 2</a></li>--}}
                    {{--</ul>--}}
                {{--</li>--}}
            </ul>

      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    

    <!-- Main content -->
    <section class="content container-fluid">

    @yield('content')

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <!-- Default to the left -->
    <strong>Copyright &copy; 2018 <a href="#">Scliniq</a>.</strong> All rights reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
  
    <!-- Tab panes -->
  
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
  immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 3 -->
<script src={{asset("bower_components/jquery/dist/jquery.min.js")}}></script>
<!-- Bootstrap 3.3.7 -->
<script src={{asset("bower_components/bootstrap/dist/js/bootstrap.min.js")}}></script>
<!-- AdminLTE App -->
<script src={{asset("dist/js/adminlte.min.js")}}></script>

<!-- DataTables -->
<script src={{asset("bower_components/datatables.net/js/jquery.dataTables.min.js")}}></script>
<script src={{asset("bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js")}}></script>

<!-- Select2 -->
<script src={{asset("bower_components/select2/dist/js/select2.full.min.js")}}></script>

<!-- bootstrap time picker -->
<script src={{asset("plugins/timepicker/bootstrap-timepicker.min.js")}}></script>

<!-- bootstrap datepicker -->
<script src={{asset("bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js")}}></script>

<script>
 $(function () {
        $('#example1').DataTable();
        $('#example2').DataTable();

       $('.timepicker').timepicker({
            showInputs: false,
            defaultTime: false
        });

         //Date picker
    $('.datepicker').datepicker({
      autoclose: true,
      format : 'yyyy-mm-dd',
    })
})

 var timeout = 9000; //milliseconds

//this method will be called every 300 milliseconds
setInterval(ajaxRequest, timeout);


var url = '<?= url('/notification') ?>';
// this method contain your ajax request
function ajaxRequest() { //function to ajax request
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
        },
        type: "GET",
        url: url,

    }).done(function(result){
        console.log(result);
       
    });
}

</script>


@yield('scripts')
</body>
</html>