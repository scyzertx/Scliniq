@extends('layouts.admin_layout')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Add medicine Schedule</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="/medicines">
                        {{ csrf_field() }}
                       
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Medicine Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        

                        

                        <div class="form-group{{ $errors->has('start_date') ? ' has-error' : '' }}">
                            <label for="start_date" class="col-md-4 control-label">Start Date</label>

                            <div class="col-md-6">
                                <input id="start_date" type="text" class="form-control datepicker" name="start_date" value="{{ old('start_date') }}" required autofocus>

                                @if ($errors->has('start_date'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('start_date') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                         <div class="form-group{{ $errors->has('end_date') ? ' has-error' : '' }}">
                            <label for="end_date" class="col-md-4 control-label">End Date</label>

                            <div class="col-md-6">
                                <input id="end_date" type="text" class="form-control datepicker" name="end_date" value="{{ old('end_date') }}" required autofocus>

                                @if ($errors->has('end_date'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('end_date') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('time') ? ' has-error' : '' }}">
                            <label for="time" class="col-md-4 control-label">Time</label>

                            <div class="col-md-6 bootstrap-timepicker" id="add-time" >
                                <input id="time" type="text" class="form-control timepicker" style="margin-bottom:10px" name="time[]" value="{{ old('time') }}" required >

                            

                                

                                @if ($errors->has('time'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('time') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="col-md-2">
                                <button type="button"  class="form-control btn btn-success btn-sm" id="add" >Add</button>
                                <button type="button" class='form-control btn btn-danger btn-sm' id="remove" >Remove</button>
                            </div>


                        </div>


                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                        Add medicine
                                </button>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script>
        $('body').on('click' , '#add' , function(){
            $("#add-time").append('<input id="time" type="text" class="form-control timepicker" style="margin-bottom:10px" name="time[]" value="{{ old('time') }}" required >');
            
            $('.timepicker').timepicker({
            showInputs: false,
            defaultTime: false
        });
        });

        $('body').on('click' , '#remove' , function(){
            var count = $('#add-time input').length;
            if(count > 1 ){
                $('#add-time input:last-child').remove();
            }
        });
    </script>
@endsection
