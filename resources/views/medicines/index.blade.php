@extends('layouts.admin_layout')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Medicine Schedule
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Medicine Schedule</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="box">
                    <div class="box-header">
                        
                        <small style="margin-left: 15px">
                       
                            <a href={{route('medicines.create')  }}>
                                <button type="submit" class="btn btn-success">Add Medicine</button>
                            </a>
                            
                        </small>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive">
                       
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>S.N.</th>
                                <th>Medicine Name</th>
                                <th>Time</th>
                                <th>Start Date</th>
                                <th>End Date</th>
                           </tr>
                            </thead>
                            <tbody>

                           @php($i=1)
                                @foreach($medicines as $medicine)
                                <tr>
                                    @foreach($medicine->schedules as $medicine_schedule)

                                        <td>{{$i}}</td>                                       
                                        <td>{{$medicine->name}}</td>
                                        <td>{{$medicine_schedule->time}}</td>
                                        <td>{{$medicine->start_date}}</td>
                                        <td>{{$medicine->end_date}}</td>
                                  
                                        <td>
                                            <div class="btn-group">
                                               @if(Auth::user()->admin==0)
                                                <a href={{route('medicines.edit' ,  ['medicine' =>$medicine->id])  }}>
                                                    <button type="submit" class="btn btn-primary">Edit</button>
                                                </a>
                                                <form action={{route('medicines.destroy' ,['medicine'=>$medicine_schedule->id])}} method="POST"
                                                      style="display: inline">
                                                    {{csrf_field()}}
                                                    {{method_field("DELETE")}}
                                                    <button type="submit" class="btn btn-danger"
                                                            onclick="return confirm('Are you sure you want to delete?')">
                                                          
                                                        Delete
                                                    </button>
                                                </form>
                                               
                                            @endif
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                    @php($i++)
                                @endforeach

                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>

        </div>
    </section>
    <!-- /.content -->

@endsection
