@section('styles')
    <style>
        #myMap {
            height: 400px;
            width: auto;
        }
    </style>
@endsection
{{ csrf_field() }}
<div class="row">
    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
        <div class="{{$errors->has('name')?'form-group has-error':'form-group'}}">
            {{Form::label('name' , '1. Doctor Name' )}}
            {{Form::text('name' , null , ['class'=>'form-control' , 'placeholder'=>'Doctor Name' , 'required'])}}
            @if ($errors->has('name'))
                <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
        <div class="{{$errors->has('address')?'form-group has-error':'form-group'}}">
            {{Form::label('address' , '2. Address' )}}
            {{Form::text('address' , null , ['class'=>'form-control', 'id'=> 'address' , 'placeholder'=>'address' , 'required'])}}
            {{Form::hidden('latitude' , null , ['class'=>'form-control' , 'id'=>'latitude'])}}
            {{Form::hidden('longitude' , null , ['class'=>'form-control' , 'id' => 'longitude'])}}
            @if ($errors->has('address'))
                <span class="help-block">
                    <strong>{{ $errors->first('address') }}</strong>
                </span>
            @endif
        </div>
    </div>
</div>

<div class="row" style="margin-bottom: 20px;">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    	<div id="myMap">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
        <div class="{{$errors->has('contact')?'form-group has-error':'form-group'}}">
            {{Form::label('contact' , '3. Contact Number' )}}
            {{Form::text('contact' , null , ['class'=>'form-control' , 'placeholder'=>'Contact number' , 'required'])}}
            @if ($errors->has('contact'))
                <span class="help-block">
                    <strong>{{ $errors->first('contact') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
        <div class="{{$errors->has('image_file')?'form-group has-error':'form-group'}}">
            {{Form::label('image_file' , '4. Image' )}}
            @if(isset($Doctor))
                <p><img src={{url('images/'.$Doctor->image)}} alt="{{$Doctor->name}}" width="100px"
                        height="100px"></p>
            @endif
            {{Form::file('image_file' , ['class'=>''])}}
            @if ($errors->has('image_file'))
                <span class="help-block">
                    <strong>{{ $errors->first('image_file') }}</strong>
                </span>
            @endif
        </div>
    </div>

</div>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="{{$errors->has('features')?'form-group has-error':'form-group'}}">
            {{Form::label('features' , '5. Features' )}}
            {{Form::textarea('features' , null , ['class'=>'form-control' , 'placeholder'=>'e.g. Wifi,TV,Bar'])}}
            @if ($errors->has('features'))
                <span class="help-block">
                    <strong>{{ $errors->first('features') }}</strong>
                </span>
            @endif
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <label>6.Schedule</label>
    </div>

</div>

<div class="row">
    <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">

        <div class="{{$errors->has('sunday')?'form-group has-error bootstrap-timepicker':'form-group bootstrap-timepicker'}}">
            {{Form::label('sunday' , 'Sunday' )}}
            {{Form::text('sunday[]' , !empty($schedules[0]['day']) ? $schedules[0]['opening_time'] : NULL , ['class'=>'form-control timepicker' , 'placeholder'=>'Opening Time' , 'autocomplete' => 'off'])}}
            {{Form::text('sunday[]' , !empty($schedules[0]['day']) ? $schedules[0]['closing_time'] : NULL , ['class'=>'form-control timepicker' , 'placeholder'=>'Closing Time'  , 'style' => 'margin-top:10px' ,'autocomplete' => 'off'])}}
            @if ($errors->has('sunday'))
                <span class="help-block">
                    <strong>{{ $errors->first('sunday') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
        <div class="{{$errors->has('monday')?'form-group has-error bootstrap-timepicker':'form-group bootstrap-timepicker'}}">
            {{Form::label('monday' , 'Monday' )}}
            {{Form::text('monday[]' , !empty($schedules[1]['day']) ? $schedules[0]['opening_time'] : NULL , ['class'=>'form-control timepicker' , 'placeholder'=>'Opening Time' ,'autocomplete' => 'off' ])}}
            {{Form::text('monday[]' , !empty($schedules[1]['day']) ? $schedules[0]['closing_time'] : NULL , ['class'=>'form-control timepicker' , 'placeholder'=>'Closing Time'  , 'style' => 'margin-top:10px' , 'autocomplete' => 'off'])}}
            @if ($errors->has('monday'))
                <span class="help-block">
                    <strong>{{ $errors->first('monday') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
        <div class="{{$errors->has('tuesday')?'form-group has-error bootstrap-timepicker':'form-group bootstrap-timepicker'}}">
            {{Form::label('tuesday' , 'Tuesday' )}}
            {{Form::text('tuesday[]' , !empty($schedules[2]['day']) ? $schedules[0]['opening_time'] : NULL, ['class'=>'form-control timepicker' , 'placeholder'=>'Opening Time' ])}}
            {{Form::text('tuesday[]' , !empty($schedules[2]['day']) ? $schedules[0]['closing_time'] : NULL , ['class'=>'form-control timepicker' , 'placeholder'=>'Closing Time'  , 'style' => 'margin-top:10px'])}}
            @if ($errors->has('tuesday'))
                <span class="help-block">
                    <strong>{{ $errors->first('tuesday') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
        <div class="{{$errors->has('wednesday')?'form-group has-error bootstrap-timepicker':'form-group bootstrap-timepicker'}}">
            {{Form::label('wednesday' , 'Wednesday' )}}
            {{Form::text('wednesday[]' , !empty($schedules[3]['day']) ? $schedules[0]['opening_time'] : NULL , ['class'=>'form-control timepicker' , 'placeholder'=>'Opening Time' ])}}
            {{Form::text('wednesday[]' ,!empty($schedules[3]['day']) ? $schedules[0]['closing_time'] : NULL , ['class'=>'form-control timepicker' , 'placeholder'=>'Closing Time'  , 'style' => 'margin-top:10px'])}}
            @if ($errors->has('wednesday'))
                <span class="help-block">
                    <strong>{{ $errors->first('wednesday') }}</strong>
                </span>
            @endif
        </div>
    </div>


</div>

<div class="row">

    <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
        <div class="{{$errors->has('thursday')?'form-group has-error bootstrap-timepicker':'form-group bootstrap-timepicker'}}">
            {{Form::label('thursday' , 'Thursday' )}}
            {{Form::text('thursday[]' ,!empty($schedules[4]['day']) ? $schedules[0]['opening_time'] : NULL , ['class'=>'form-control timepicker' , 'placeholder'=>'Opening Time' ])}}
            {{Form::text('thursday[]' ,!empty($schedules[4]['day']) ? $schedules[0]['closing_time'] : NULL , ['class'=>'form-control timepicker' , 'placeholder'=>'Closing Time'  , 'style' => 'margin-top:10px'])}}
            @if ($errors->has('thursday'))
                <span class="help-block">
                    <strong>{{ $errors->first('thursday') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
        <div class="{{$errors->has('friday')?'form-group has-error bootstrap-timepicker':'form-group bootstrap-timepicker'}}">
            {{Form::label('friday' , 'Friday' )}}
            {{Form::text('friday[]' , !empty($schedules[5]['day']) ? $schedules[0]['opening_time'] : NULL, ['class'=>'form-control timepicker' , 'placeholder'=>'Opening Time' ])}}
            {{Form::text('friday[]' , !empty($schedules[5]['day']) ? $schedules[0]['closing_time'] : NULL , ['class'=>'form-control timepicker' , 'placeholder'=>'Closing Time'  , 'style' => 'margin-top:10px'])}}
            @if ($errors->has('friday'))
                <span class="help-block">
                    <strong>{{ $errors->first('friday') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
        <div class="{{$errors->has('saturday')?'form-group has-error bootstrap-timepicker':'form-group bootstrap-timepicker'}}">
            {{Form::label('saturday' , 'Saturday' )}}
            {{Form::text('saturday[]' , !empty($schedules[6]['day']) ? $schedules[0]['opening_time'] : NULL, ['class'=>'form-control timepicker' , 'placeholder'=>'Opening Time' ])}}
            {{Form::text('saturday[]' ,!empty($schedules[6]['day']) ? $schedules[0]['closing_time'] : NULL , ['class'=>'form-control timepicker' , 'placeholder'=>'Closing Time'  , 'style' => 'margin-top:10px'])}}
            @if ($errors->has('saturday'))
                <span class="help-block">
                    <strong>{{ $errors->first('saturday') }}</strong>
                </span>
            @endif
        </div>
    </div>


</div>


@section('scripts')

    <script defer src="http://maps.googleapis.com/maps/api/js?key=AIzaSyA20nug2Nwl0hOe6p14F5aSk5Pz64K7qY4&libraries=places&callback=initMap"></script>

    <script type="text/javascript">

        function initMap(){
            var map;
        var marker;
        var lat =<?php if(!empty($Doctor->latitude)): echo $Doctor->latitude; else: echo 28.220918511522903 ;endif?>;
        var lng =<?php if(!empty($Doctor->longitude)): echo $Doctor->longitude; else: echo 83.99625482544548 ;endif?>;
        var myLatlng = new google.maps.LatLng(lat,lng);
        var geocoder = new google.maps.Geocoder();
        var infowindow = new google.maps.InfoWindow();
        
            var mapOptions = {
                zoom: 18,
                center: myLatlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            map = new google.maps.Map(document.getElementById("myMap"), mapOptions);

            marker = new google.maps.Marker({
                map: map,
                position: myLatlng,
                draggable: true
            });

            geocoder.geocode({'latLng': myLatlng }, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        $('#latitude,#longitude').show();
                        $('#address').val(results[0].formatted_address);
                        $('#latitude').val(marker.getPosition().lat());
                        $('#longitude').val(marker.getPosition().lng());
                        infowindow.setContent(results[0].formatted_address);
                        infowindow.open(map, marker);
                    }
                }
            });

            google.maps.event.addListener(marker, 'dragend', function() {

                geocoder.geocode({'latLng': marker.getPosition()}, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (results[0]) {
                            $('#address').val(results[0].formatted_address);
                            $('#latitude').val(marker.getPosition().lat());
                            $('#longitude').val(marker.getPosition().lng());
                            infowindow.setContent(results[0].formatted_address);
                            infowindow.open(map, marker);
                        }
                    }
                });
            });

        }
    </script>
@endsection
