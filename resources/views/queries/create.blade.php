@extends('layouts.admin_layout')

@section('content')
<div class="col-md-12">
          <!-- Box Comment -->
          <div class="box box-widget">
            <div class="box-header with-border">
              
             
            </div>

          
            <div class="box-body">
         
              <p>{{$query->queries}}</p>

        
              
              <button type="button" class="btn btn-default btn-xs"><i class="fa fa-thumbs-o-up"></i> Like</button>
              <span class="pull-right text-muted">6 likes - 2 answer</span>
            </div>
           
          @foreach($query->answers as $answer) 
            <div class="box-footer box-comments">
              <div class="box-comment">
                <!-- User image -->
                <img class="img-circle img-sm" src="../dist/img/user3-128x128.jpg" alt="User Image">

                <div class="comment-text">
                      <span class="username">
                        {{$answer->doctor_name}}
                        <button type="button" class="btn btn-default btn-xs pull-right"><i class="fa fa-thumbs-o-up"></i> Like</button><span class="pull-right text-muted">5 likes</span>
                      </span><!-- /.username -->
                  {{$answer->answers}}
                </div>
                <!-- /.comment-text -->
              </div>
              
              
              
            </div>

          @endforeach  
       
            <div class="box-footer">
              <form action="{{url('/discussion/answer')}}" method="post">
                {{csrf_field()}}
                <input type="hidden" name="query_id" value={{$query->id}}>
                <input type="hidden" name="doctor_name" value={{Auth::user()->name}}>
                <div class="form-group">
                  <textarea class="form-control" rows="3" name="answers" placeholder="Write a answer......"></textarea>
                </div>
             
                <div class="box-footer">
                  <button type="submit" class="btn btn-primary pull-right">Submit</button>
                </div>
              </form>
            </div>
        
          </div>
          <!-- /.box -->
        </div>
@endsection