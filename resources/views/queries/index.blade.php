@extends('layouts.admin_layout')

@section('content')


<div class="col-md-12">
<div class="col-md-12">
          <div class="box box-warning box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Ask Queries</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body" style="">
    
              <form action="{{url('/discussion')}}" method="post" role="form">
                {{csrf_field()}}
                  <!-- textarea -->
                  <input type="hidden" name="user_id" value={{Auth::user()->id}}>
                  <div class="form-group">
                    <textarea class="form-control" rows="3" name="queries" placeholder="Enter your query"></textarea>
                  </div>
            
              </div>
              <div class="box-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
            </form>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
          <div class="nav-tabs-custom">
           
            <div class="tab-content">
              <div class="tab-pane active" id="activity">
                <!-- Post -->
                
                @foreach($queries as $query)
                  <div class="post">
                    
                    <!-- /.user-block -->
                    <p>
                       {{$query->queries}}
                    </p>
          
                    <ul class="list-inline">
                      
                      <li><a href="#" class="link-black text-sm"><i class="fa fa-thumbs-o-up margin-r-5"></i> Like</a>
                      </li>
                      <li class="pull-right">
                        <a href="{{'/discussion/'.$query->id}}" class="link-black text-sm"><i class="fa fa-comments-o margin-r-5"></i> Answer
                          (1)</a></li>
                    </ul>

                    <input class="form-control input-sm" type="text" placeholder="Type a comment">
                  </div>
                @endforeach  
                  
                </form>
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
        @endsection
