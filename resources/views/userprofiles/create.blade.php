@extends('layouts.admin_layout')

@section('content')


<div class="box box-warning">
            <div class="box-header with-border">
              <h3 class="box-title">Profile</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <form role="form">
                <!-- text input -->
                <div class="form-group">
                  <input type="text" class="form-control" placeholder="Enter Age">
                </div>
            
              
                <!-- select -->
                <div class="form-group">
                  <label>Select Blood Group</label>
                  <select class="form-control">
                    <option>A+</option>
                    <option>A-</option>
                    <option>B+</option>
                    <option>B-</option>
                    <option>AB+</option>
                    <option>AB-</option>
                    <option>O+</option>
                    <option>O-</option>
                  </select>
                </div>

                  <div class="form-group">
                  <label>Select Gnder</label>
                  <select class="form-control">
                    <option>Male</option>
                    <option>Female</option>
                    <option>Others</option>
                      </select>
                </div>

                 <div class="form-group">
                  <input type="text" class="form-control" placeholder="Enter Height">
                </div>

                 <div class="form-group">
                  <input type="text" class="form-control" placeholder="Enter Weight">
                </div>

                <div class="form-group">
                  <label>Marital Status</label>
                  <select class="form-control">
                    <option>Married</option>
                    <option>Unmarried</option>
                    <option>Widowed</option>
                    <option>Separated</option>
                      </select>
                </div>
             
            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
              </form>
            </div>
            <!-- /.box-body -->
          </div>
@endsection
