@extends('layouts.admin_layout')

@section('content')

<div class="box-body box-profile">
              <img class="profile-user-img img-responsive img-circle" src="../../dist/img/user4-128x128.jpg" alt="User profile picture">

              <h3 class="profile-username text-center">Nikesh Ahikari</h3>

              <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                  <b>Age</b> <a class="pull-right">22</a>
                </li>
                <li class="list-group-item">
                  <b>Blood Group</b> <a class="pull-right">A+</a>
                </li>
                <li class="list-group-item">
                  <b>Gender</b> <a class="pull-right">Male</a>
                </li>
                <li class="list-group-item">
                  <b>Height</b> <a class="pull-right">137m</a>
                </li>
                <li class="list-group-item">
                  <b>Weight</b> <a class="pull-right">60kg</a>
                </li>
                <li class="list-group-item">
                  <b>Marital Status</b> <a class="pull-right">Unmarried</a>
                </li>
              </ul>

              <a href="#" class="btn btn-primary btn-block"><b>Update</b></a>
            </div>

            <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">About Me</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <strong><i class="fa fa-book margin-r-5"></i> Email</strong>

              <p class="text-muted">
                Nikesh@gmail.com
              </p>

              <hr>

              <strong><i class="fa fa-map-marker margin-r-5"></i> Location</strong>

              <p class="text-muted">Fulbari , Pokhara</p>

               <strong><i class="fa fa-map-marker margin-r-5"></i> Phone Number</strong>

<p class="text-muted">9817115775</p>


      
            </div>
            <!-- /.box-body -->
          </div>

            @endsection