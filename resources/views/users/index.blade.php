@extends('layouts.admin_layout')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Users
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">User List</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Users List</h3>
                        <small style="margin-left: 15px">
                            <a href={{route('users.create')  }}>
                            @if(Auth::user()->admin == 1 )
                                <button type="submit" class="btn btn-success">Add New</button>
                            @endif 
                            </a>
                        </small>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive">
                       
                        <table id="example2" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>S.N.</th>
                                <th>Username</th>
                                <th>Email</th>
                                <th>Phone Number</th>
                                <th>Role</th>
                                <th>Action</th>
                             </tr>
                            </thead>
                            <tbody>

                           @php($i=1)
                                @foreach($users as $user)
                                    <tr>
                                        <td>{{$i}}</td>
                                        <td>{{$user->name}}</td>
                                        <td>{{$user->email}}</td>
                                        <td>{{$user->phone_number}}</td>
                                        <td>
                                            @if($user->admin == 1)
                                                <span class="label label-success">Super Admin</span>
                                            @elseif($user->admin == 2)
                                                <span class="label label-primary">Admin</span>
                                            @elseif($user->admin == 3)
                                            <span class="label label-danger">Doctor</span>
                                            @else
                                            <span class="label label-info">User</span>
                                            @endif
                                        </td>
                                       
                                        <td>
                                            <div class="btn-group">

                                               
                                                <a href={{route('users.edit' ,  ['user' =>$user->id])  }}>
                                                    <button type="submit" class="btn btn-primary">Edit</button>
                                                </a>
                                                <form  method="POST"
                                                      style="display: inline" action={{route('users.destroy' ,['user'=>$user->id])}} >
                                                    {{csrf_field()}}
                                                    {{method_field("DELETE")}}
                                                    <button type="submit" class="btn btn-danger"
                                                            onclick="return confirm('Are you sure you want to delete?')">
                                                          
                                                        Delete
                                                    </button>
                                                </form>

                                            </div>
                                        </td>
                                    </tr>
                                    @php($i++)
                                @endforeach

                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>

        </div>
    </section>
    <!-- /.content -->

@endsection
