<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/register', "Api\RegisterController@create");
Route::middleware('auth:api')->get('/current_user', function (Request $request) {
    return $request->user();
});

Route::post('password/email' , 'Auth\ForgotPasswordController@sendResetLinkEmail');

Route::middleware('auth:api')->resource('/firstaids', "FirstaidController");

Route::get('/hospital/departments/doctors/{id}' , 'HospitalController@show_hospital_departments_doctors');

Route::get('/hospital/departments/{id}' , 'HospitalController@show_hospital_departments');

Route::get('/details/{id}' , 'DoctorController@show_details')->middleware('auth:api');

Route::middleware('auth:api')->resource('/users', "UserController");

Route::middleware('auth:api')->resource('/medicines', "MedicineController");
Route::middleware('auth:api')->post('/medicines/user' , 'MedicineController@user_medicine');
Route::middleware('auth:api')->post('/medicines/time' , 'MedicineController@medicine_time');

Route::middleware('auth:api')->resource('/hospitals', "HospitalController");

Route::middleware('auth:api')->resource('/departments' , "DepartmentController");

Route::middleware('auth:api')->resource('/doctors', "DoctorController");

Route::middleware('auth:api')->resource('/appointments', "AppointmentController");

Route::middleware('auth:api')->get('/appointment/all' , 'DoctorController@appointments_all');

Route::middleware('auth:api')->get('/appointment/details/{id}' , 'AppointmentController@appointment_show_details');

Route::middleware('auth:api')->post('/specific/appointments' , 'AppointmentController@specific_appointments');

Route::middleware('auth:api')->resource('/discussion' , 'QueryController');

Route::middleware('auth:api')->post('/discussions/answer' , 'QueryController@store_answer');

Route::middleware('auth:api')->resource('/schedule' , 'ScheduleController');

Route::middleware('auth:api')->post('/doctor/hospital/details' , 'ScheduleController@doctor_hospitals');
Route::middleware('auth:api')->post('/doctor/schedule/details' , 'ScheduleController@doctor_schedule_details');