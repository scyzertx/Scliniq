<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/





Route::group(['middleware' => ['web','auth']], function(){
    

    Route::get('/', function(){
        //super-admin=1 admin=2 doctor=3 
        if(Auth::user()->admin ==1 || Auth::user()->admin == 3 ){
            $doctor = \App\Doctor::all()->count();
            // $department = \App\Department::all()->count();

            $line_charts_array = array();
            for($i = 1 ; $i<=8 ; $i++){
                $date = \Carbon\Carbon::now()->addDays($i);
                $appointment_count= \App\Appointment::where('date', $date->format('Y-m-d'))->count();
                $line_chart['period'] = $date->format('Y-m-d');
                $line_chart['appointments'] = $appointment_count;
                array_push($line_charts_array , $line_chart);

            }

        $line_charts = json_encode($line_charts_array);
                return view('home' , compact('doctor' , 'line_charts'));
        }
        elseif (Auth::user()->admin == 2 ) {
            $users['users']= \App\User::all();
            $doctor = \App\Doctor::where('hospital_id' , \Auth::user()->hospital_id)->count();
            $department = \App\Department::where('hospital_id' , \Auth::user()->hospital_id)->count();
            $appointment = \App\Appointment::where('hospital_id' , \Auth::user()->hospital_id)->count();
            $firstaid =\App\Firstaid::all()->count();
            
            $line_charts_array = array();
                for($i = 1 ; $i<=8 ; $i++){
                    $date = \Carbon\Carbon::now()->addDays($i);
                    $appointment_count= \App\Appointment::where('hospital_id', Auth::user()->hospital_id)->where('date', $date->format('Y-m-d'))->count();
                    $line_chart['period'] = $date->format('Y-m-d');
                    $line_chart['appointments'] = $appointment_count;
                    array_push($line_charts_array , $line_chart);

                }

            $line_charts = json_encode($line_charts_array);
      

                return view('home', compact('users' , 'doctor' , 'firstaid', 'department', 'appointment', 'line_charts'));
            
            } else{
                //normal user 0
                return view('layouts.admin_layout');    
            }
    });

});


//user verification route
Route::get('/user/verify/{token}' , 'Auth\RegisterController@verifyUser')->middleware('auth');
Route::get('/resend/{id}' , 'Auth\RegisterController@resend')->middleware('auth');

Route::get('/hospital/departments', 'HospitalController@show_departments')->middleware('auth');
Route::get('/hospital/doctors' , 'HospitalController@show_doctors')->middleware('auth');

Route::get('/hospital/departments/{id}' , 'HospitalController@show_hospital_departments')->middleware('auth');
Route::resource('/hospitals', "HospitalController")->middleware('auth');

Route::resource('/doctors', "DoctorController")->middleware('auth');

Route::get('/doctors/hospitals/{id}' , 'DoctorController@show_all')->middleware('auth');

Route::get('/hospital/departments/doctors/{id}' , 'HospitalController@show_hospital_departments_doctors')->middleware('auth');
Route::get('/hospital/doctors/appointment/{id}' , 'AppointmentController@show_appointment')->middleware('auth');
Route::get('/appointments/hospitals/{id}' , 'AppointmentController@show_all')->middleware('auth');
Route::resource('/appointments', "AppointmentController")->middleware('auth');
// Route::get('/appointments//doctors')

Route::resource('/users', "UserController")->middleware('auth');

Route::get('/test' , function(){
    return view('queries.index');
});

Route::resource('/medicines', "MedicineController")->middleware('auth');

//list of user for super admin
Route::resource('user_details', "UserController")->middleware('auth');

//user profile detail
Route::resource('/profile', "UserDetailController")->middleware('auth');

//for storing answe
Route::post('/discussion/answer' , 'QueryController@store_answer')->middleware('auth');
Route::resource('/discussion', "QueryController")->middleware('auth');

Route::resource('/departments' , "DepartmentController")->middleware('auth');

Route::resource('/firstaids', "FirstaidController")->middleware('auth');
// Route::get('/firstaids/details' , 'FirstaidController@details');

Route::resource('/emergency_contact', "EmergencyController")->middleware('auth');

Route::resource('/schedule' , 'ScheduleController')->middleware('auth');
//for shifts
Route::post('/shifts' , 'ScheduleController@show_schedule')->middleware('auth');


Route::get('/check' , function(){
    $hospitals = App\Hospital::all();
    return view('doctors.set_schedule' , compact('hospitals'))->middleware('auth');
});

Route::get('/test/{token}', 'Api\RegisterController@sendAppointmentNotification')->middleware('auth');

Auth::routes();

Route::get('/details/{id}' , 'DoctorController@show_details')->middleware('auth');

Route::get('/appointment/details/{id}' , 'AppointmentController@appointment_show_details')->middleware('auth');

Route::post('/specific/appointments' , 'AppointmentController@specific_appointments')->middleware('auth');

Route::get('/doctor/schedule/details' , 'ScheduleController@doctor_schedule_details')->middleware('auth');

Route::get('/notification' , 'HomeController@notification')->middleware('auth');


